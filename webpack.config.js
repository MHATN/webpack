const path = require("path")
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin

module.exports = {
    entry: "./src/app.js",
    output: {
        filename: "yeay.js",
        path: path.resolve(__dirname, "build")
    },
    module:{
        rules:[
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    "css-loader"
                ]
            }
        ]
    },
    plugins: [
        new BundleAnalyzerPlugin()
    ]
}